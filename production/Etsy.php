<?php

/**
 * Etsy Post and Get Order PHP class
 *
 * @version 0.1
 */
require_once("vendor2/autoload.php");

class Etsy {


	function postproduct($project,$store,$appdata)
	{


			$etsy_oauth_token = $store_value['member_store_key'];
			$etsy_oauth_token_secret = $store_value['member_store_key'];
			$etsy_store_name = $store_value['member_store_key'];

			//check keyword and add it to array

			$oauth = new OAuth($appdata['etsy_keystring'], $appdata['etsy_shared_secret']);

			$oauth->setToken($etsy_oauth_token, $etsy_oauth_token_secret);

			try {

				$url = "https://openapi.etsy.com/v2/listings";

				if($project['etsy_draft'] == "on"){

					// publish
					$params = array( 'quantity' => $project['stock'],
				                 'title' => $project['gb_title'],
				                 'description' => $project['gb_description'],
				                 'price' => $project['gb_cost'],
				                 'taxonomy_id' => $taxonomy_id,
				                 'non_taxable' => true,
				                 'state' => 'active',
				                 'who_made' => 'i_did',
				                 'is_supply' => 'false',
				                 //'is_customizable' => false,
				                 'when_made' => '2020_2020',
				                 'recipient' => 'not_specified',
				                 "shipping_template_id" =>$project['rest_api_category_id'],
				                 //"processing_min" => 1,
                                 //"processing_max" => 10,
                                 "occasion" => 'birthday',
                                 "tags" => array($tag_data),
                                 "materials" => array($material),
                                 "sku" => array($project['gb_id'])
				        );

				}else{

					// draft
					$params = array( 'quantity' => $project['stock'],
				                 'title' => $project['gb_title'],
				                 'description' => $project['gb_description'],
				                 'price' => $project['gb_cost'],
				                 'taxonomy_id' => $taxonomy_id,
				                 'non_taxable' => true,
				                 'state' => 'draft',
				                 'who_made' => 'i_did',
				                 'is_supply' => 'false',
				                 //'is_customizable' => false,
				                 'when_made' => '2020_2020',
				                 'recipient' => 'not_specified',
				                 "shipping_template_id" =>$project['rest_api_category_id'],
				                 //"processing_min" => 1,
                                 //"processing_max" => 10,
                                 "occasion" => 'birthday',
                                 "tags" => array($tag_data),
                                 "materials" => array($material),
                                 "sku" => array($project['gb_id'])
				        );

				}




				$oauth->fetch($url, $params, OAUTH_HTTP_METHOD_POST);
				$json = $oauth->getLastResponse();
				$response_etsy_data = json_decode($json, true);


				//post image to etsy
				$arr_json = json_decode($json);

				foreach($arr_json->results as $value){
					//print_r($value);
					$listing_id  = $value->listing_id;
				}

				$client = new Etsy\EtsyClient($appdata['etsy_keystring'],$appdata['etsy_shared_secret']);
				$client->authorize($etsy_oauth_token, $etsy_oauth_token_secret);

				$api = new Etsy\EtsyApi($client);

				$listing_image = array(
						'params' => array(
							'listing_id' => $listing_id
						),
						'data' => array(
							'image' => array('@'.$img.';type=image/png')
				));

				$api->uploadListingImage($listing_image);

                // ================= START UPDATED BY KOPKAP ================= //

                // Get Properties From API
                $taxomony_id = 432;
                $url = "https://openapi.etsy.com/v2/taxonomy/seller/".$taxomony_id."/properties";

                $oauth->fetch($url, OAUTH_HTTP_METHOD_GET);
                $json = $oauth->getLastResponse();
                $properties = json_decode($json, true);

                $colors = [];

                foreach($properties["results"] as $property) {
                    if ($property["name"] === "Primary color") {
                        $colors["id"] = $property["property_id"];
                        $colors["possible_values"] = $property["possible_values"];
                    }
                }

                $black = $this->select($colors["possible_values"], "name", "Black");

				$product = [
                    'sku' => $project['gb_sku'],
                    'property_values' => array($black),
                    'offerings' => [
                        0 => [
                            'price' => $project['gb_cost'],
                            'quantity' => $project['stock'],
                        ],
                    ],
                ];


				$args = array(
					'params' => array(
						'listing_id' => $listing_id
					),
					'data' => array(
						'products' => array(
							'json' => json_encode([
				                0 => $product,
				            ])
						),
                        'price_on_property' => [$colors["id"]],
                        'quantity_on_property' => [$colors["id"]],
                        'sku_on_property' => [$colors["id"]],
					)
				);

                // ================= END UPDATED BY KOPKAP ================= //

		        $etsy_inventory_items = $api->updateInventory($args);

				if(file_exists($img)){
					unlink($img);
				}

				return $arr_json;


			}catch (OAuthException $e) {
				return $e->getMessage();
			}


	}

	private function select($items, $key, $value) {
	    return  array_filter(array_keys($items), function ($item) use ($value, $key) {
	        return $item[$key] = $value;
	    });
    }
}
