<?php

/**
 * Etsy Post and Get Order PHP class
 *
 * @version 0.1
 */
require_once("vendor/autoload.php");

use App\Etsy;
use App\Project;
use Dotenv\Dotenv;

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author  Mike van Riel <me@mikevanriel.com>
 */
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */

$consumerKey = $_ENV["CONSUMER_KEY"];
$consumerSecret = $_ENV["CONSUMER_SECRET"];
$accessToken = $_ENV["ACCESS_TOKEN"];
$accessSecret = $_ENV["ACCESS_SECRET"];

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */
$etsy = new Etsy();

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */
$etsy->authorize($consumerKey, $consumerSecret, $accessToken, $accessSecret);

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */

$project = new Project();

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */
$id = $etsy->createListing(
    $project->info(),
    $project->taxonomyID(),
    $project->tags(),
    $project->materials()
);

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */
$etsy->uploadListingImage($id, $project->image());

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */
$etsy->uploadListingImage($id, $project->image());

/**
 * This is the summary for a DocBlock.
 *
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
 */
$etsy->addProductToListing($id, $project->products(), $project->properties());
