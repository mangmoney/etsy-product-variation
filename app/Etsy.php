<?php

/**
 * Etsy Post and Get Order PHP class
 *
 * @version 0.1
 */
namespace App;

use Etsy\EtsyClient;
use Etsy\EtsyApi;

class Etsy
{
    private $api;

    /**
     * This is the summary for a DocBlock.
     *
     * @param $key
     * @param $secret
     * @param $accessToken
     * @param $accessSecret
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     */
    public function authorize($key, $secret, $accessToken, $accessSecret)
    {
        $client = new EtsyClient($key, $secret);
        $client->authorize($accessToken, $accessSecret);

        $this->api = new EtsyApi($client);
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @param $project
     * @param $taxonomyID
     * @param $tags
     * @param $materials
     * @return object
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function createListing($project, $taxonomyID, $tags, $materials)
    {
        $data = array(
            "quantity" => $project["stock"],
            "title" => $project["gb_title"],
            "description" => $project["gb_description"],
            "price" => $project["gb_cost"],
            "taxonomy_id" => $taxonomyID,
            "non_taxable" => true,
            "state" => $project["etsy_draft"],
            "who_made" => "i_did",
            "is_supply" => "false",
            "when_made" => "2020_2020",
            "recipient" => "not_specified",
            "shipping_template_id" => $project["rest_api_category_id"],
            "occasion" => "birthday",
            "tags" => array($tags),
            "materials" => array($materials),
            "sku" => array($project["gb_id"])
        );

        $args = array(
            "data" => $data
        );

        return $this->api->listings($args);
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @param $listingID
     * @param $image
     * @return object
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function uploadListingImage($listingID, $image) {
        $params = array(
            "listing_id" => $listingID
        );

        $data = array(
            "image" => array("@".$image.";type=image/png")
        );

        $args = array(
            "params" => $params,
            "data" => $data,
        );

        return $this->api->uploadListingImage($args);
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @param $listingID
     * @param $products
     * @return object
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     */
    public function addProductToListing($listingID, $products, $properties) {
        $params = array(
            "listing_id" => $listingID,
        );

        $data = array(
            "products" => array(
                "json" => json_encode($products),
            ),
            'price_on_property' => $properties,
            'quantity_on_property' => $properties,
            'sku_on_property' => $properties,
        );

        $args = array(
            "params" => $params,
            "data" => $data,
        );

        return $this->api->updateInventory($args);
    }
}
