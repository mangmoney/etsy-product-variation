<?php

/**
 * Etsy Post and Get Order PHP class
 *
 * @version 0.1
 */
namespace App;

class Project {
    public int $colorID = 1;
    public int $sizeID = 2;

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function info() {
        return array(
            "gb_id" => "sku",
            "gb_title" => "Project Name",
            "gb_description" => "Project Description",
            "gb_code" => 2000,
            "rest_api_category_id" => 1,
            "stock" => 1,
            "etsy_draft" => "draft"
        );
    }

    public function properties() {
        return [$this->colorID, $this->sizeID];
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function products() {
        $color = $this->color();
        $size = $this->size();

        return array(
            array(
                "sku" => "shirt",
                "property_values" => array(
                    $color["yellow"],
                    $size["M"],
                ),
                "offerings" => array(
                    array(
                        "price" => 10,
                        "quantity" => 3
                    ),
                ),
            ),
        );
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function color() {
       return array(
           "yellow" => array(
               "property_id"   => $this->colorID,
               "property_name" => "color",
               "values"        => ["Yellow"],
           ),
           "black" => array(
               "property_id"   => $this->colorID,
               "property_name" => "color",
               "values"        => ["Black"],
           ),
       );
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function size() {
        return array(
            "S" => array(
                "property_id"   => $this->sizeID,
                "property_name" => "size",
                "values"        => ["S"],
            ),
            "M" => array(
                "property_id"   => $this->sizeID,
                "property_name" => "size",
                "values"        => ["M"],
            ),
            "XL" => array(
                "property_id"   => $this->sizeID,
                "property_name" => "size",
                "values"        => ["XL"],
            ),
        );
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function image() {
        return "https://scontent.fbkk13-2.fna.fbcdn.net/v/t1.0-9/44974760_1295620260579740_7097871010728247296_n.jpg";
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function taxonomyID() {
        return 1;
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function tags() {
        return "tags";
    }

    /**
     * This is the summary for a DocBlock.
     *
     * @since 1.0
     * @author Sarayut Lawilai <imkopkap.dev@gmail.com>
     *
     */
    public function materials() {
        return "materials";
    }
}
